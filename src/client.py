import random
import argparse
import time
import boto3

from pydantic import BaseModel
from pydantic.utils import List
from paho.mqtt.client import Client


class Output(BaseModel):
    OutputKey: str
    OutputValue: str


class Stack(BaseModel):
    StackId: str
    StackName: str
    Description: str
    Outputs: List[Output] = None
    StackStatus: str


def on_connect(client, userdata, flags, rc):
    print("Client connected")


def on_message(client, userdata, msg):
    print(f'message received on topic {msg.topic} with content "{msg.payload}"')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--server',
                        default='ecs',
                        choices=['local', 'ecs'],
                        type=str,
                        help='deploy mode')

    args = parser.parse_args()

    client = Client(client_id='my_first_stream')
    client.on_connect = on_connect
    client.on_message = on_message

    if args.server == 'local':
        client.connect('localhost', port=1883)
        print(f'Dashboard is running at http://localhost:8080')

    elif args.server == 'ecs':
        b_client = boto3.client('cloudformation')

        response = b_client.describe_stacks(StackName='vpc-ecs')
        stack = Stack(**response['Stacks'].pop(0))
        if stack.StackStatus != 'CREATE_COMPLETE':
            raise ValueError(f'the stack status is "{stack.StackStatus}". '
                             f'create the stack_vpc_ecs before stack_mqtt.')

        response = b_client.describe_stacks(StackName='mqtt')
        stack = Stack(**response['Stacks'].pop(0))
        if stack.StackStatus != 'CREATE_COMPLETE':
            raise ValueError(f' "{stack.StackStatus}". '
                             f'wait some more minutes for the stack to complete')

        if stack.Outputs is None:
            raise ValueError('the mqtt stack is still not up. wait a couple of minutes')
        for output in stack.Outputs:
            if output.OutputKey == 'BrokerDNS':
                dns = output.OutputValue
                client.connect(dns, port=1883)
            if output.OutputKey == 'DashboardDNS':
                print(f'Dashboard is running at http://{output.OutputValue} \n  Default login is admin:hivemq')

    client.loop_start()

    while True:
        client.publish(topic="stream_topic", payload=f'a random number {random.uniform(1, 1000)}')
        # let's send approx 10 messages per second
        time.sleep(0.1)


